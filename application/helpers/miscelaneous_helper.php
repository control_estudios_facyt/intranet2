<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// PARA AGREGAR LOS MENSAJES EN LOS FLASH DATA
function set_message($type = NULL, $message = NULL, $icon = NULL)
{
	$CI = & get_instance();
	$type = strtolower($type);
	$icon = strtolower($icon);

	if ($type == NULL || $message == NULL){echo "Usted no ha especificado ningún message ó type"; die(); }
    if($icon == NULL){
        if($type == 'success' || $type == 'exito'){
            $mensaje = "<div class='alert alert-success well-sm text-center' role='alert'><i class='fa fa-check fa-1-5x pull-left'></i>".$message."<br></div>";
        }elseif($type == 'danger' || $type == 'error'){
            $mensaje = "<div class='alert alert-danger well-sm text-center' role='alert'><i class='fa fa-times fa-1-5x pull-left'></i>".$message."<br></div>";
        }elseif($type == 'warning' || $type == 'precaucion'){
            $mensaje = "<div class='alert alert-warning well-sm text-center' role='alert'><i class='fa fa-hand-pointer-o fa-1-5x pull-left'></i>".$message."<br></div>";
        }elseif($type == 'info' || $type == 'info'){
            $mensaje = "<div class='alert alert-info well-sm text-center' role='alert'><i class='fa fa-info-circle fa-1-5x pull-left'></i>".$message."<br></div>";
        }
    }else{
        if($type == 'success' || $type == 'exito'){
            $mensaje = "<div class='alert alert-success well-sm text-center' role='alert'><i class='fa ".$icon." fa-1-5x pull-left'></i>".$message."<br></div>";
        }elseif($type == 'danger' || $type == 'error'){
            $mensaje = "<div class='alert alert-danger well-sm text-center' role='alert'><i class='fa ".$icon." fa-1-5x pull-left'></i>".$message."<br></div>";
        }elseif($type == 'warning' || $type == 'precaucion'){
            $mensaje = "<div class='alert alert-warning well-sm text-center' role='alert'><i class='fa ".$icon." fa-1-5x pull-left'></i>".$message."<br></div>";
        }elseif($type == 'info' || $type == 'info'){
            $mensaje = "<div class='alert alert-info well-sm text-center' role='alert'><i class='fa ".$icon." fa-1-5x pull-left'></i>".$message."<br></div>";
        }
    }

	$CI->session->set_flashdata('mensaje', $mensaje);
}