<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	/* Carga la vista del login */
	function index(){
		$data['error'] = $this->session->flashdata('error');

		$this->load->view('login/headers');
		$this->load->view('login/index', $data);
		$this->load->view('login/footer');
	}
	
	/* Verificar los datos de acceso del usuario en la base de datos */
	function verificar()
	{
		if(sizeof($_POST) == 0){
			// redirijo a login
            set_message('danger', "No puede acceder así al sistema");
			redirect('login/index');
		}else{
			set_message('info', $_POST['usuario']." ".$_POST['contraseña']);
			redirect('login/index');
		}

	}

	function olivido_clave_formulario()
	{
		// echo "estoy en olvidar clave formulario";
		$this->load->view('login/headers');
		$this->load->view('login/olvido_clave');
		$this->load->view('login/footer');
	}

	function olvido_clave_paso1()
	{
		// envio los datos introducidos y verifico que existan, sino existen retorno con un mensaje de error a esta vista
		print_r($_POST);
		$usuario = $_POST['usuario'];
		// verifico en la base de datos si existe el nombre de usuario o correo que introdujo
		// si existe lo paso el siguiente paso donde le pido otro datos, como por ejemplo la fecha de nacimiento
		// verifico la fecha de nacimiento si existe le doy acceso al cambio de clave, si no al inicio.
	}

}
	
?>