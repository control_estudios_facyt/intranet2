<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$view['script']='<script src="'.base_url().'assets/js/chart-master/Chart.js"></script>';
		$this->load->view('inicio/headers', $view);
		$this->load->view('inicio/bienvenido');
		$this->load->view('inicio/footer');
	}

	function acerca_de()
	{
		$this->load->view('plantilla/header_simple');
		$this->load->view('inicio/acerca_de');
		$this->load->view('plantilla/footer_simple_w_logos');
	}
}
	
?>