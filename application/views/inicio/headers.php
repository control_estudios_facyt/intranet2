<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<!-- <meta name="author" content="Dashboard"> -->
	<!-- <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina"> -->

	<link rel="shortcut icon" href="<?php echo base_url('assets/img/ico-facyt.png'); ?>" type="image/png" />
	<title>Portal Intranet de la FACyT</title>


	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
	<!--external css-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome-470/css/font-awesome.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/zabuto_calendar.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/gritter/css/jquery.gritter.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lineicons/style.css') ?>">
	
	<!-- Custom styles for this template -->
	<link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style-responsive.css') ?>" rel="stylesheet">
	<?php if(!empty($link)) {echo $link;} ?>
	<?php if(!empty($script)) {echo $script;} ?>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	  <![endif]-->
  </head>

  <body>
	<section id="container" >
		<header class="header black-bg">
			<div class="sidebar-toggle-box">
				<div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
			</div>
			<!--logo start-->
			<a href="<?php base_url('inicio') ?>" class="logo"><b>Intranet FACyT</b></a>
			<!--logo end-->
			<!--menu notificaciones star-->

			<div class="top-menu">
				<ul class="nav pull-right top-menu">
					<li><a class="logout" href="<?php echo base_url('login') ?>">Salir <i class="fa fa-sign-out"></i></a></li>
				</ul>
			</div>
		</header>
		<!--header end-->


		<aside>
			<div id="sidebar"  class="nav-collapse ">
				<!-- sidebar menu start-->
				<ul class="sidebar-menu" id="nav-accordion">

					<p class="centered"><a href="#"><img src="<?php echo base_url('assets/img/LOGO_FACYT.jpg'); ?>" class="img-circle" width="60"></a></p>
					<h5 class="centered">FACYT</h5>

					<li class="mt">
						<a class="active" href="<?php echo site_url('inicio') ?>">
							<i class="fa fa-home"></i>
							<span>Inicio</span>
						</a>
					</li>

					<li>
						<a href="index.html">
							<i class="fa fa-chevron-right"></i>
							<span>Servicios</span>
						</a>
					</li>

					<li>
						<a href="index.html">
							<i class="fa fa-chevron-right"></i>
							<span>Admin</span>
						</a>
					</li>

					<li>
						<a href="index.html">
							<i class="fa fa-chevron-right"></i>
							<span>Contactos</span>
						</a>
					</li>

					<li>
						<a href="<?php echo site_url('acerca_de') ?>">
							<i class="fa fa-chevron-right"></i>
							<span>Acerca de</span>
						</a>
					</li>

					<!-- <li class="sub-menu">
						<a href="javascript:;" >
							<i class="fa fa-desktop"></i>
							<span>UI Elements</span>
						</a>
						<ul class="sub">
							<li><a  href="general.html">General</a></li>
							<li><a  href="buttons.html">Buttons</a></li>
							<li><a  href="panels.html">Panels</a></li>
						</ul>
					</li>

					<li class="sub-menu">
						<a href="javascript:;" >
							<i class="fa fa-cogs"></i>
							<span>Components</span>
						</a>
						<ul class="sub">
							<li><a  href="calendar.html">Calendar</a></li>
							<li><a  href="gallery.html">Gallery</a></li>
							<li><a  href="todo_list.html">Todo List</a></li>
						</ul>
					</li>
					<li class="sub-menu">
						<a href="javascript:;" >
							<i class="fa fa-book"></i>
							<span>Extra Pages</span>
						</a>
						<ul class="sub">
							<li><a  href="blank.html">Blank Page</a></li>
							<li><a  href="login.html">Login</a></li>
							<li><a  href="lock_screen.html">Lock Screen</a></li>
						</ul>
					</li> -->
					<!-- <li class="sub-menu">
						<a href="javascript:;" >
							<i class="fa fa-tasks"></i>
							<span>Forms</span>
						</a>
						<ul class="sub">
							<li><a  href="form_component.html">Form Components</a></li>
						</ul>
					</li>
					<li class="sub-menu">
						<a href="javascript:;" >
							<i class="fa fa-th"></i>
							<span>Data Tables</span>
						</a>
						<ul class="sub">
							<li><a  href="basic_table.html">Basic Table</a></li>
							<li><a  href="responsive_table.html">Responsive Table</a></li>
						</ul>
					</li> -->
					<!-- <li class="sub-menu">
						<a href="javascript:;" >
							<i class=" fa fa-bar-chart-o"></i>
							<span>Charts</span>
						</a>
						<ul class="sub">
							<li><a  href="morris.html">Morris</a></li>
							<li><a  href="chartjs.html">Chartjs</a></li>
						</ul>
					</li> -->


					<li>
						<a href="<?php echo site_url('cerrar-sesion') ?>">
							<i class="fa fa-sign-out"></i>
							<span>Cerrar Sesión</span>
						</a>
					</li>


					<li class="no-margin">
						<!-- CALENDAR-->
						<div id="calendar" class="mb">
							<div class="panel green-panel no-margin">
								<div class="panel-body no-padding">
									<div title="" data-original-title="" id="date-popover" class="popover top" style="cursor: pointer; margin-left: 33%; margin-top: -50px; width: 175px; display: none;">
										<div class="arrow"></div>
										<h3 class="popover-title" style="disadding: none;"></h3>
										<div id="date-popover-content" class="popover-content"></div>
									</div>
									<div id="zabuto_calendar_1bso"><div class="zabuto_calendar" id="zabuto_calendar_1bso"><table class="table"><tbody><tr class="calendar-month-header"><th><div id="zabuto_calendar_1bso_nav-prev" class="calendar-month-navigation"><span><span class="fa fa-chevron-left text-transparent"></span></span></div></th><th colspan="5"><span>April 2017</span></th><th><div id="zabuto_calendar_1bso_nav-next" class="calendar-month-navigation"><span><span class="fa fa-chevron-right text-transparent"></span></span></div></th></tr><tr class="calendar-dow-header"><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></tr><tr class="calendar-dow"><td></td><td></td><td></td><td></td><td></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-01"><div id="zabuto_calendar_1bso_2017-04-01_day" class="day">1</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-02"><div id="zabuto_calendar_1bso_2017-04-02_day" class="day">2</div></td></tr><tr class="calendar-dow"><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-03"><div id="zabuto_calendar_1bso_2017-04-03_day" class="day">3</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-04"><div id="zabuto_calendar_1bso_2017-04-04_day" class="day">4</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-05"><div id="zabuto_calendar_1bso_2017-04-05_day" class="day">5</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-06"><div id="zabuto_calendar_1bso_2017-04-06_day" class="day">6</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-07"><div id="zabuto_calendar_1bso_2017-04-07_day" class="day">7</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-08"><div id="zabuto_calendar_1bso_2017-04-08_day" class="day">8</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-09"><div id="zabuto_calendar_1bso_2017-04-09_day" class="day">9</div></td></tr><tr class="calendar-dow"><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-10"><div id="zabuto_calendar_1bso_2017-04-10_day" class="day">10</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-11"><div id="zabuto_calendar_1bso_2017-04-11_day" class="day">11</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-12"><div id="zabuto_calendar_1bso_2017-04-12_day" class="day">12</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-13"><div id="zabuto_calendar_1bso_2017-04-13_day" class="day">13</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-14"><div id="zabuto_calendar_1bso_2017-04-14_day" class="day">14</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-15"><div id="zabuto_calendar_1bso_2017-04-15_day" class="day">15</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-16"><div id="zabuto_calendar_1bso_2017-04-16_day" class="day">16</div></td></tr><tr class="calendar-dow"><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-17"><div id="zabuto_calendar_1bso_2017-04-17_day" class="day">17</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-18"><div id="zabuto_calendar_1bso_2017-04-18_day" class="day">18</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-19"><div id="zabuto_calendar_1bso_2017-04-19_day" class="day">19</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-20"><div id="zabuto_calendar_1bso_2017-04-20_day" class="day">20</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-21"><div id="zabuto_calendar_1bso_2017-04-21_day" class="day">21</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-22"><div id="zabuto_calendar_1bso_2017-04-22_day" class="day">22</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-23"><div id="zabuto_calendar_1bso_2017-04-23_day" class="day">23</div></td></tr><tr class="calendar-dow"><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-24"><div id="zabuto_calendar_1bso_2017-04-24_day" class="day">24</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-25"><div id="zabuto_calendar_1bso_2017-04-25_day" class="day">25</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-26"><div id="zabuto_calendar_1bso_2017-04-26_day" class="day">26</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-27"><div id="zabuto_calendar_1bso_2017-04-27_day" class="day">27</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-28"><div id="zabuto_calendar_1bso_2017-04-28_day" class="day">28</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-29"><div id="zabuto_calendar_1bso_2017-04-29_day" class="day">29</div></td><td class="dow-clickable" id="zabuto_calendar_1bso_2017-04-30"><div id="zabuto_calendar_1bso_2017-04-30_day" class="day">30</div></td></tr></tbody></table><div class="legend" id="zabuto_calendar_1bso_legend"><span class="legend-text"><span class="badge badge-event">00</span> Special event</span><span class="legend-block"><ul class="legend"><li class="event"></li><span>Regular event</span></ul></span></div></div></div>
								</div>
							</div>
						</div><!-- / calendar -->
					</li>
				</ul>
			</div>

		</aside>
		<!--sidebar end-->
		<section id="main-content">