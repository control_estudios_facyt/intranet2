<section class="wrapper">

	<div class="row">
		<div class="col-lg-12 main-chart">

			<!-- <div class="row mtbox">
				<div class="col-md-2 col-sm-2 col-md-offset-1 box0">
					<div class="box1">
						<span class="li_heart"></span>
						<h3>933</h3>
					</div>
					<p>933 People liked your page the last 24hs. Whoohoo!</p>
				</div>
				<div class="col-md-2 col-sm-2 box0">
					<div class="box1">
						<span class="li_cloud"></span>
						<h3>+48</h3>
					</div>
					<p>48 New files were added in your cloud storage.</p>
				</div>
				<div class="col-md-2 col-sm-2 box0">
					<div class="box1">
						<span class="li_stack"></span>
						<h3>23</h3>
					</div>
					<p>You have 23 unread messages in your inbox.</p>
				</div>
				<div class="col-md-2 col-sm-2 box0">
					<div class="box1">
						<span class="li_news"></span>
						<h3>+10</h3>
					</div>
					<p>More than 10 news were added in your reader.</p>
				</div>
				<div class="col-md-2 col-sm-2 box0">
					<div class="box1">
						<span class="li_data"></span>
						<h3>OK!</h3>
					</div>
					<p>Your server is working perfectly. Relax &amp; enjoy.</p>
				</div>

			</div> --><!-- /row mt -->	

			<div class="row">
				
				<!-- <div class="col-md-3 col-sm-3 mb">
					<div class="white-panel pn donut-chart">
						<div class="white-header">
							<h5>SERVER LOAD</h5>
						</div>
						<div class="row">
							<div class="col-sm-6 col-xs-6 goleft">
								<p><i class="fa fa-database"></i> 70%</p>
							</div>
						</div>
						<canvas style="width: 120px; height: 120px;" id="serverstatus01" height="120" width="120"></canvas>
						<script>
							var doughnutData = [
							{
								value: 70,
								color:"#68dff0"
							},
							{
								value : 30,
								color : "#fdfdfd"
							}
							];
							var myDoughnut = new Chart(document.getElementById("serverstatus01").getContext("2d")).Doughnut(doughnutData);
						</script>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 mb">
					<div class="white-panel pn">
						<div class="white-header">
							<h5>TOP PRODUCT</h5>
						</div>
						<div class="row">
							<div class="col-sm-6 col-xs-6 goleft">
								<p><i class="fa fa-heart"></i> 122</p>
							</div>
							<div class="col-sm-6 col-xs-6"></div>
						</div>
						<div class="centered">
							<img src="assets/img/product.png" width="120">
						</div>
					</div>
				</div>
				<div class="col-md-3 mb">
					<div class="white-panel pn">
						<div class="white-header">
							<h5>TOP USER</h5>
						</div>
						<p><img src="assets/img/ui-zac-2.jpg" class="img img-responsive"></p>
						<p><b>Zac Snider</b></p>
						<div class="row">
							<div class="col-md-6">
								<p class="small mt">MEMBER SINCE</p>
								<p>2012</p>
							</div>
							<div class="col-md-6">
								<p class="small mt">TOTAL SPEND</p>
								<p>$ 47,60</p>
							</div>
						</div>
					</div>
				</div> 
				<div class="col-md-3 mb">
					<div class="white-panel pn">
						<div class="white-header">
							<h5></h5>
						</div>
						<p></p>
					</div>
				</div> -->

				<!-- kill em with kindness -->
				
				<div class="col-md-3 mb">
					<div class="white-panel pn">
						<div class="white-header">
							<h5>Sistema de Asistencia</h5>
						</div>
						<p><img src="assets/img/ui-zac-2.jpg" class="img img-responsive"></p>
						<p>Registro de asistencia para el personal Docente, Administrativo y Obrero</p>
					</div>
				</div>

				<div class="col-md-3 mb">
					<div class="white-panel pn">
						<div class="white-header">
							<h5>Agenda Telefonica</h5>
						</div>
						<p><img src="assets/img/ui-zac-2.jpg" class="img img-responsive"></p>
						<p>Agenda telefonica FACYT</p>
					</div>
				</div>
				
				<!-- Soporte Tecnico -->
				<div class="col-md-3 mb">
					<div class="white-panel pn">
						<div class="white-header">
							<h5>Soporte Técnico</h5>
						</div>
						<p><img src="assets/img/ui-zac-2.jpg" class="img img-responsive"></p>
						<p>Soporte y asistencia técnica <a href="#">TUTORIAL HELPDESK FACYT</a></p>
					</div>
				</div>

				<div class="col-md-3 mb">
					<div class="white-panel pn">
						<div class="white-header">
							<h5>Cambio de Clave de Servidor Alfa</h5>
						</div>
						<p><img src="assets/img/ui-zac-2.jpg" class="img img-responsive"></p>
						<p>Cambie su clave en el servidor ALFA de la FACyT</p>

					</div>
				</div>
			</div>

			<div class="row">
				<!-- Reservaciones -->
				<div class="col-md-3 mb">
					<div class="white-panel pn">
						<div class="white-header">
							<h5>Reservación Laboratorios</h5>
						</div>
						<p><img src="assets/img/ui-zac-2.jpg" class="img img-responsive"></p>
						<ul class="list-unstyled">
							<li><a class="btn btn-default btn-sm mb-xs" href="#">Docencia Computación</a></li>
							<li><a class="btn btn-default btn-sm mb-xs" href="#">Edificio de Aulas</a></li>
							<li><a class="btn btn-default btn-sm mb-xs" href="#">Dpto. Biología</a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-3 mb">
					<div class="white-panel pn">
						<div class="white-header">
							<h5>Reservación Salas</h5>
						</div>
						<p><img src="assets/img/ui-zac-2.jpg" class="img img-responsive"></p>
						<ul class="list-unstyled">
							<li><a class="btn btn-default btn-sm mb-xs" href="#">Tesis (Edificio Computación)</a></li>
							<li><a class="btn btn-default btn-sm mb-xs" href="#">LDAP (Edificio Computación)</a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-3 mb">
					<div class="white-panel pn">
						<div class="white-header">
							<h5>Reservación Equipos</h5>
						</div>
						<p><img src="assets/img/ui-zac-2.jpg" class="img img-responsive"></p>
						<ul class="list-unstyled">
							<li><a class="btn btn-default btn-sm mb-xs" href="#">Decanato</a></li>
							<li><a class="btn btn-default btn-sm mb-xs" href="#">Biblioteca</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 mb">
					<div class="white-panel pn">
						<div class="white-header">
							<h5>Otras Reservaciones</h5>
						</div>
						<p><img src="assets/img/ui-zac-2.jpg" class="img img-responsive"></p>
						<ul class="list-unstyled">
							<li><a class="btn btn-default btn-sm mb-xs" href="#">Auditorio Mariana Souto</a></li>
							<li><a class="btn btn-default btn-sm mb-xs" href="#">Equipos Audiovisuales</a></li>
							<li><a class="btn btn-default btn-sm mb-xs" href="#">Aulas</a></li>
						</ul>
					</div>
				</div>

				
			</div><!-- /row -->


			<!-- <div class="row">
				<div class="col-md-4 mb">
					<div class="darkblue-panel pn">
						<div class="darkblue-header">
							<h5>DROPBOX STATICS</h5>
						</div>
						<canvas style="width: 120px; height: 120px;" id="serverstatus02" height="120" width="120"></canvas>
						<script>
							var doughnutData = [
							{
								value: 60,
								color:"#68dff0"
							},
							{
								value : 40,
								color : "#444c57"
							}
							];
							var myDoughnut = new Chart(document.getElementById("serverstatus02").getContext("2d")).Doughnut(doughnutData);
						</script>
						<p>April 17, 2014</p>
						<footer>
							<div class="pull-left">
								<h5><i class="fa fa-hdd-o"></i> 17 GB</h5>
							</div>
							<div class="pull-right">
								<h5>60% Used</h5>
							</div>
						</footer>
					</div>
				</div>


				<div class="col-md-4 mb">
					<div class="instagram-panel pn">
						<i class="fa fa-instagram fa-4x"></i>
						<p>@THISISYOU<br>
							5 min. ago
						</p>
						<p><i class="fa fa-comment"></i> 18 | <i class="fa fa-heart"></i> 49</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 mb">
					<div class="darkblue-panel pn">
						<div class="darkblue-header">
							<h5>REVENUE</h5>
						</div>
						<div class="chart mt">
							<div class="sparkline" data-type="line" data-resize="true" data-height="75" data-width="90%" data-line-width="1" data-line-color="#fff" data-spot-color="#fff" data-fill-color="" data-highlight-line-color="#fff" data-spot-radius="4" data-data="[200,135,667,333,526,996,564,123,890,464,655]"><canvas height="75" width="250" style="display: inline-block; width: 250px; height: 75px; vertical-align: top;"></canvas></div>
						</div>
						<p class="mt"><b>$ 17,9125</b><br>Month Income</p>
					</div>
				</div>
			</div> -->

			<!-- <div class="row mt">
				<div class="border-head">
					<h3>VISITS</h3>
				</div>
				<div class="custom-bar-chart">
					<ul class="y-axis">
						<li><span>10.000</span></li>
						<li><span>8.000</span></li>
						<li><span>6.000</span></li>
						<li><span>4.000</span></li>
						<li><span>2.000</span></li>
						<li><span>0</span></li>
					</ul>
					<div class="bar">
						<div class="title">JAN</div>
						<div style="height: 85%;" class="value tooltips" data-original-title="8.500" data-toggle="tooltip" data-placement="top"></div>
					</div>
					<div class="bar ">
						<div class="title">FEB</div>
						<div style="height: 50%;" class="value tooltips" data-original-title="5.000" data-toggle="tooltip" data-placement="top"></div>
					</div>
					<div class="bar ">
						<div class="title">MAR</div>
						<div style="height: 60%;" class="value tooltips" data-original-title="6.000" data-toggle="tooltip" data-placement="top"></div>
					</div>
					<div class="bar ">
						<div class="title">APR</div>
						<div style="height: 45%;" class="value tooltips" data-original-title="4.500" data-toggle="tooltip" data-placement="top"></div>
					</div>
					<div class="bar">
						<div class="title">MAY</div>
						<div style="height: 32%;" class="value tooltips" data-original-title="3.200" data-toggle="tooltip" data-placement="top"></div>
					</div>
					<div class="bar ">
						<div class="title">JUN</div>
						<div style="height: 62%;" class="value tooltips" data-original-title="6.200" data-toggle="tooltip" data-placement="top"></div>
					</div>
					<div class="bar">
						<div class="title">JUL</div>
						<div style="height: 75%;" class="value tooltips" data-original-title="7.500" data-toggle="tooltip" data-placement="top"></div>
					</div>
				</div>
			</div> --><!-- /row -->	

		</div><!-- /col-lg-9 END SECTION MIDDLE -->

</div><!-- --/row ---->
</section>