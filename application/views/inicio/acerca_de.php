<style>
	ul li { list-style: disc; }
</style>
<section class="wrapper">
	<div class="col-lg-12">
		<h1 class="text-info text-center">Acerca de</h1>
		<hr>
		<div class="row">
			<div class="col-lg-6">
				<h2>Área de Servicios</h2>
				<!-- <hr> -->
				<p>El trabajo en esta área, esta dirigido a garantizar la disponibilidad y uso de la información, a través de los medios y componentes de conexión en Redes, administrando los recursos asociados a los servicios de información y ofreciendo apoyo técnico para la resolución de problemas en equipamientos, interconexiones, implementación, y actualización de los sistemas operativos atacando los problemas de seguridad informática. Además dándole importancia al factor humano a través del adiestramiento tanto presencial como remoto en el uso de los sistemas de información.</p>
			</div>
			<div class="col-lg-6">
				<h2>Área de Sistemas</h2>
				<!-- <hr> -->
				<p>En esta se debe cumplir con la implementación de productos informáticos que pueden ser de desarrollo propio o por convenio de alianza con productos ya desarrollados, coordinando las diferentes actividades técnicas y administrativas que permitan ejecutar los procesos de prestación de servicios, brindando el apoyo mediante la Mecanización, Sistematización y Automatización de la información en ambientes Intranet, Extranet e Internet. Esto permite a las autoridades el control, gestión y desarrollo de los sistemas, en las áreas académicas, administrativas y de servicio, para lograr el cumplimiento de su misión.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<h2>¿Por qué Intranet FACYT?</h2>
				<!-- <hr> -->
				<ul>
					<li>Mejora las comunicaciones</li>
					<li>Incentiva la colaboración</li>
					<li>Elimina de duplicidad confusa de información</li>
					<li>Mantiene la información precisa y actualizada</li>
					<li>La información es más fácilmente distribuida y rápida</li>
					<li>Administración y Control de los diferentes sistemas de la UST</li>
					<li>Su estrategia de cómputo distribuido utiliza los recursos de cómputo más efectivamente</li>
					<li>Interfase común de información y comunicación</li>
					<li>Resguardo y seguridad de la información (usuarios, virus, etc)</li>
				</ul>
			</div>
			<div class="col-lg-6">
				<p></p>
				<p>
					<img src="<?php echo base_url('assets/img/organigrama-ust.png') ?>" alt="Estructura UST" class="img-responsive img-center">
				</p>
			</div>
		</div>
	</div>
</section>