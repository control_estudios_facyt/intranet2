<style type="text/css">
	.alert { border-radius: 0px; }
	.alert {
	  padding: 15px;
	  margin-bottom: 0px;
	  border: 1px solid transparent;
	}
	.alert-info {
	  color: #31708f;
	  background-color: #d9edf7;
	  border-color: #bce8f1;
	}
</style>
<div id="login-page">
	<div class="container">

		<form method="POST" class="form-login" action="<?php echo base_url('login/olvido_clave_paso1') ?>">

			<h2 class="form-login-heading">
				Recuperar Acceso a su Cuenta
			</h2>
			
			<?php if ($this->session->flashdata('mensaje') != FALSE) { echo $this->session->flashdata('mensaje'); } ?>

			<div class="alert alert-info">
				<h3 class="text-center">Paso 1 de 3</h3>
				<p>Por favor complete todos los campos para comprobar su vinculaciòn con la cuenta.</p>
			</div>
			<div class="login-wrap">
				<input name="usuario" type="text" class="form-control" placeholder="Usuario o correo electronico" required="required" autocomplete="off">
				<br>
	
				<button class="btn btn-theme btn-block" href="principal.html" type="submit">Enviar Datos</button>

			</div>
		</form>	  	
	</div>
</div>