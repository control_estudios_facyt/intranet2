<style type="text/css">
	.alert { border-radius: 0px; }
	.registration { margin-top: 15px; }
</style>
<div id="login-page">
	<div class="container">

		<form method="POST" class="form-login" action="<?php echo base_url('login/verificar') ?>">

			<h2 class="form-login-heading">
				<img src="<?php echo base_url('assets/img/telematica.png') ?>" class="img-responsive" style="margin-bottom: 5px;">
				Identificación de Usuario
			</h2>
			
			<?php if ($this->session->flashdata('mensaje') != FALSE) { echo $this->session->flashdata('mensaje'); } ?>

			<div class="login-wrap">
				<input name="usuario" type="text" class="form-control" placeholder="Usuario" required="required">
				<br>
				<input name="contraseña" type="password" class="form-control" placeholder="Contraseña">
				<label class="checkbox">
					<span class="pull-right">
						<a data-toggle="modal" href="<?php echo site_url('olvido_clave/formulario'); ?>"> ¿Olvido su contraseña?</a>
					</span>
				</label>
				<button class="btn btn-theme btn-block" href="principal.html" type="submit"><i class="fa fa-lock"></i> Entrar</button>

				<!--<div class="login-social-link centered">
				<p>or you can sign in via your social network</p>
				<button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Facebook</button>
				<button class="btn btn-twitter" type="submit"><i class="fa fa-twitter"></i> Twitter</button>
				</div>-->
				<div class="registration">
					<!-- Bienvenidos al portal Intranet de la FACYT. Utilice su información personal de Alfa para tener acceso al sistema. <br/> -->
					<!-- Don't have an account yet?<br/> -->
					<a href="<?php echo base_url('acerca_de') ?>">Acerca de</a> | <a class="" href="#"> Cambiar Contraseña</a>
				</div>
			</div>
		</form>	  	
	</div>
</div>
