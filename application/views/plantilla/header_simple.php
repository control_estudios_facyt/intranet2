<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="DTIC - FACYT">
	<meta name="keyword" content="">

	<link rel="shortcut icon" href="<?php echo base_url('assets/img/ico-facyt.png'); ?>" type="image/png" />
	<title>Portal Intranet de la FACyT</title>

	<link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style-responsive.css') ?>" rel="stylesheet">
	<?php if(!empty($link)) {echo $link;} ?>
	<?php if(!empty($script)) {echo $script;} ?>

</head>
<body>
	<section id="container">
		<header class="header black-bg">
			<!--logo start-->
			<a href="<?php echo base_url('inicio') ?>" class="logo"><b>Intranet FACyT</b></a>
			<!--logo end-->
			<!--menu notificaciones star-->

			<div class="top-menu">
				<ul class="nav pull-right top-menu">
					<li><a class="logout" href="<?php echo base_url('inicio') ?>">Inicio <i class="fa fa-home fa-fw"></i></a></li>
				</ul>
			</div>
		</header>
		<!--header end-->

